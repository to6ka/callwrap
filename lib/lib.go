package lib

import (
	"context"
	"time"
)

type WrapFunc func(call CallParams)

type CallParams struct {
	Name    string
	Start   time.Time
	Context context.Context
	Error   error
}
