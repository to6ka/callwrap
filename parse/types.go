package parse

import (
	"fmt"
	"path"
	"strings"
)

type FileData struct {
	Package     string
	UsedImports Imports
	Interfaces  []Interface
}

type Imports map[string]Import

type Import struct {
	Name string
	Path string
}

func (i Import) String() string {
	if i.Name == path.Base(i.Path) {
		return fmt.Sprintf(`"%s"`, i.Path)
	}
	return fmt.Sprintf(`%s "%s"`, i.Name, i.Path)
}

type Interface struct {
	Name    string
	Methods []Method
}

type Method struct {
	Name       string
	InputArgs  Args
	OutputArgs Args
}

func (m Method) String() string {
	if len(m.OutputArgs) == 0 {
		return fmt.Sprintf("%s(%s)", m.Name, m.InputArgs)
	}
	return fmt.Sprintf("%s(%s) (%s)", m.Name, m.InputArgs, m.OutputArgs)
}

func (m Method) ContextArg(is Imports) string {
	for _, arg := range m.InputArgs {
		if arg.Type.Name == "Context" &&
			is[arg.Type.Package].Path == "context" {
			return arg.Name
		}
	}
	return ""
}

func (m Method) ErrorArg() string {
	for _, arg := range m.OutputArgs {
		if arg.Name == "err" && arg.Type.String() == "error" {
			return "err"
		}
	}
	for _, arg := range m.OutputArgs {
		if arg.Type.String() == "error" {
			return arg.Name
		}
	}
	return ""
}

type Args []Arg

func (as Args) String() string {
	argStrings := make([]string, 0, len(as))
	for _, arg := range as {
		argStrings = append(argStrings, arg.String())
	}

	return strings.Join(argStrings, ", ")
}

func (as Args) Names() string {
	argStrings := make([]string, 0, len(as))
	for _, arg := range as {
		argStrings = append(argStrings, arg.Name)
	}

	return strings.Join(argStrings, ", ")
}

type Arg struct {
	Name string
	Type Type
}

func (a Arg) String() string {
	return fmt.Sprintf("%s %s", a.Name, a.Type)
}

type Type struct {
	Name    string
	Package string
	Starred int
}

func (t Type) star() string {
	ret := ""
	for i := 0; i < t.Starred; i++ {
		ret += "*"
	}
	return ret
}

func (t Type) String() string {
	if t.Package == "" {
		return fmt.Sprintf("%s%s", t.star(), t.Name)
	}
	return fmt.Sprintf("%s%s.%s", t.star(), t.Package, t.Name)
}
