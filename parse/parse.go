package parse

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"strings"

	"github.com/pkg/errors"
)

func CollectFileData(path string) (*FileData, error) {
	fset := token.NewFileSet()

	f, err := parser.ParseFile(fset, path, nil, parser.ParseComments)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse file %s", path)
	}
	//log.Print("ast tree:", spew.Sdump(f))

	v := ifaceVisitor{
		allImports: map[string]Import{},
	}
	ast.Walk(&v, f)

	ret := FileData{
		Package:     v.pkg,
		UsedImports: Imports{},
		Interfaces:  v.interfaces,
	}

	for _, iface := range ret.Interfaces {
		for _, method := range iface.Methods {

			for _, arg := range method.InputArgs {
				if arg.Type.Package == "" {
					continue
				}

				imp, ok := v.allImports[arg.Type.Package]
				if !ok {
					panic(fmt.Sprintf("not found import for arg %s", arg))
				}

				ret.UsedImports[imp.Name] = imp
			}

			for _, arg := range method.OutputArgs {
				if arg.Type.Package == "" {
					continue
				}

				imp, ok := v.allImports[arg.Type.Package]
				if !ok {
					panic(fmt.Sprintf("not found import for arg %s", arg))
				}

				ret.UsedImports[imp.Name] = imp
			}
		}
	}

	return &ret, nil
}

type ifaceVisitor struct {
	foundRequestedDecl bool

	pkg        string
	interfaces []Interface
	allImports map[string]Import
}

func (v *ifaceVisitor) Visit(node ast.Node) ast.Visitor {
	//log.Print("visit node:", spew.Sdump(node))

	switch node := node.(type) {
	case *ast.File:
		v.pkg = node.Name.String()

	case *ast.ImportSpec:
		i := getImport(node)
		v.allImports[i.Name] = i

	case *ast.GenDecl:
		v.foundRequestedDecl = findLineInComments(node.Doc, "gen:callwrap")

	case *ast.TypeSpec:
		iFace, isIface := node.Type.(*ast.InterfaceType)

		if v.foundRequestedDecl && isIface {
			v.interfaces = append(v.interfaces, Interface{
				Name:    node.Name.String(),
				Methods: collectInterfaceMethods(iFace),
			})
		}
	}

	return v
}

func findLineInComments(comments *ast.CommentGroup, line string) bool {
	if comments == nil {
		return false
	}

	for _, comment := range comments.List {
		text := comment.Text
		text = strings.TrimPrefix(text, "//")
		text = strings.TrimSpace(text)

		if text == line {
			return true
		}
	}

	return false
}

type Methods []Method

func (m *Methods) Add(add ...Method) {
	*m = append(*m, add...)
}

func collectInterfaceMethods(iFace *ast.InterfaceType) Methods {
	ret := make(Methods, 0, len(iFace.Methods.List))

	for _, method := range iFace.Methods.List {
		if f, ok := method.Type.(*ast.FuncType); ok {
			name := method.Names[0].Name
			ret.Add(collectMethodData(name, f))
		}
	}

	return ret
}

func collectMethodData(name string, method *ast.FuncType) Method {
	ret := Method{
		Name:       name,
		InputArgs:  make([]Arg, 0, len(method.Params.List)),
		OutputArgs: make([]Arg, 0, len(method.Results.List)),
	}

	for i, param := range method.Params.List {
		ret.InputArgs = append(ret.InputArgs, getArg(i, param))
	}
	for i, param := range method.Results.List {
		ret.OutputArgs = append(ret.OutputArgs, getArg(i, param))
	}

	return ret
}
