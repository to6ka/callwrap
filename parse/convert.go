package parse

import (
	"fmt"
	"go/ast"
	"path"
	"strings"

	"github.com/pkg/errors"
)

func getArg(i int, f *ast.Field) Arg {
	typ := getType(f.Type)

	var name string
	switch {
	case len(f.Names) > 0 && f.Names[0].Name != "_":
		name = f.Names[0].Name
	case typ.String() == "error":
		name = "err"
	default:
		name = fmt.Sprintf("arg%d", i)
	}

	return Arg{
		Name: name,
		Type: typ,
	}
}

func getType(t ast.Expr) Type {
	switch t := t.(type) {
	case *ast.SelectorExpr:
		return Type{
			Name:    fmt.Sprint(t.Sel),
			Package: fmt.Sprint(t.X),
		}
	case *ast.Ident:
		return Type{
			Name: t.String(),
		}
	case *ast.StarExpr:
		typ := getType(t.X)
		typ.Starred++
		return typ
	default:
		panic(errors.Errorf("unknown type: %T", t))
	}
}

func getImport(i *ast.ImportSpec) Import {
	ret := Import{
		Path: strings.Trim(i.Path.Value, `"`),
	}

	if i.Name != nil {
		ret.Name = i.Name.String()
	} else {
		ret.Name = path.Base(ret.Path)
	}

	return ret
}
