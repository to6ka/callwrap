package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/to6ka/callwrap/generate"
	"gitlab.com/to6ka/callwrap/parse"
)

var outPath = flag.String("out", "", "path to file be generated")

func main() {
	flag.Parse()

	in := os.Getenv("GOFILE")
	out := *outPath
	if out == "" {
		out = path.Base(in)
		out = strings.TrimSuffix(out, ".go")
		out = fmt.Sprintf("%s_callwrap_generated.go", out)
	}
	log.Printf("%s -> %s processing", in, out)

	fileStat, err := os.Stat(in)
	if err != nil {
		log.Fatalf("failed to access file %s: %s", in, err)
	}
	if !fileStat.Mode().IsRegular() {
		log.Fatalf("file %s is not a regular file: %s", in, err)
	}

	data, err := parse.CollectFileData(in)
	if err != nil {
		log.Fatalf("failed to collect ifaces from file %s: %s", in, err)
	}

	file, err := openWriteFile(out)
	if err != nil {
		log.Fatalf("failed to open file %s for write: %s", out, err)
	}

	err = generate.WriteTemplate(file, out, *data)
	if err != nil {
		log.Fatalf("failed to write template: %s", err)
	}

	log.Printf("%s -> %s ok", in, out)
}

func openWriteFile(path string) (*os.File, error) {
	dir := filepath.Dir(path)

	err := createDir(dir)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create dir %s", dir)
	}

	return os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0640)
}

func createDir(dir string) error {
	dirStat, err := os.Stat(dir)
	if err == nil {
		if dirStat.IsDir() {
			return nil
		}
		return errors.Wrap(err, "exists, but not a directory")
	}

	if strings.Contains(err.Error(), "no such file or directory") {
		return os.MkdirAll(dir, 0750)
	}

	return errors.Wrap(err, "can not stat dir path")
}
