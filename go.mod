module gitlab.com/to6ka/callwrap

go 1.13

require (
	github.com/pkg/errors v0.9.1
	golang.org/x/tools v0.0.0-20200417140056-c07e33ef3290
)
